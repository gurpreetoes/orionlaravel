<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\EmailSendRequest;
class EmailSendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'to'=>'required',
            'subject'=>'required',
            'message'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'to.required' => 'Atleast one Email address is required!',
            'subject.required' => 'subject is required!',
            'message.required' => 'Message is required!'
        ];
    }
}
