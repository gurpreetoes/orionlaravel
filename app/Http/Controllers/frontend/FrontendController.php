<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
class FrontendController extends Controller
{
    public function index(){
        return view('frontend.home');
    }
    public function getCat(){   
        $categories = Category::getcat();
         return view('frontend.category', compact('categories'));
   }
   public function catDetail($category_name){
      // dd($category_name);
         $categories = Category::getcat();
         //dd($categories);
        // $catdetail = Category::find($category_name);
         $catdetail=Category::where('category_name',$category_name)->get();
        //  dd($catdetail);
         return view('frontend.catdetail')->with('categories', $categories)->with('catdetail', $catdetail);
   }

   
}
