<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Email;
use App\Http\Requests\EmailSendRequest;

class EmailController extends Controller
{
    public function index() 
   {
       $data=email::paginate(5);
        return view('admin.email.index')->with('dd',$data);
   }
   public function sendEmail() 
   {
        return view('admin.email.send');
   }

   public function store(EmailSendRequest $request) 
   { 
   
  $value= email::storeEmail($request);
   if($value==0)
   {
     $d=email::all();
     return view('admin.email.index',compact('d'))->with('email not sent', 'Email Send!');
   }
   else
   {
   $d=email::all();
    return view('admin.email.index',compact('d'))->with('success', 'Email Send!');
   }
   }
}
