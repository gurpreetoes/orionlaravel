<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UserStoreRequest;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
      
        public function dashboard(){
            return view('admin.dashboard');       
        }
        function store(UserStoreRequest $request)
            {
                User::storeUser($request);
                return Redirect('/admin/users/');
            }
        public function register()
            {
                return view('admin.users.register');
            }
        public function displayuser()
            {
                $user= User::where('roleId', '2')->Paginate(5);
                return view('admin.users.index',compact('user'));
            }
         public function destroy($id)
            {
                $categories = User::destroyUser($id); 
                return redirect('/admin/users')->with('success', 'User deleted!');
            }

}
