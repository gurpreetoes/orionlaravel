<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
   
    public function index(){   
        $categories = Category::Paginate(5);
        return view('admin.category.index', compact('categories'));

    }
   public function create(){   
        return view('admin.category.create');
    }

   public function store(CategoryRequest $request)
   {
      // dd($request);
     $categories = Category::storeCat($request);
     return redirect('/admin/category')->with('success', 'Category saved!');
   }

   public function edit($id)
   {
       $category = Category::find($id);
     //  dd($category);
       return view('admin.category.edit', compact('category'));        
   }

   public function update(CategoryRequest $request, $id)
    {
        $categories = Category::updateCat($request,$id);       
        //dd($category);
        return redirect('/admin/category')->with('success', 'Category updated!');
    }
    public function destroy($id)
    {
        $categories = Category::destroyCat($id); 
        return redirect('/admin/category')->with('success', 'Category deleted!');
    }

}
