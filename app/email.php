<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mail;

class email extends Model
{
    protected $fillable = [
        'to','subject','image','message',
    ];
    protected $table='emails';
    public static function storeEmail(EmailSendRequest $request)
    {      
  
       /// Create a new file for validate.
      
        $validated = $request->validated();
          $to = $request->get('to');
          $subject = $request->get('subject');
          $emails =  explode(",",$to);               
          $img = $request->file('image');
          
          if (!empty($request->file('image'))) {
          $newname= $img->getClientOriginalName();   //get image name
          $path= $img->getRealPath();
          }else{
            $newname ='';
            $path = '';
          }
          //dd($request->all());
         
         $data = array(
             
             'subject' => $request->get('subject'),
             'msg' => $request->get('message')
         );
         //dd($data);
         $saveemail = new email([
            'to' =>  $request->get('to'),
            'subject' => $request->get('subject'),
            'image' =>$newname,
            'message' => $request->get('message')
         ]);
         Mail::send('admin/template/emailtemplate',$data,function($msg)use ($emails,$path,$newname,$subject){
            $msg->to($emails)->subject($subject); 

            if (!empty($path)) {
               // $message->attach($data['attachment']);
           
            $msg->attach($path, [
                'as' => $newname,
                'mime' => 'image/png/pdf',
            ]);
        }
            
            });
            if (Mail::failures()) {
              
                return 0;            
               } else {
                   if(!empty($newname)){
                $img->move(public_path('images'),$newname);
                   }
                   $saveemail->save();
                   return 1;
               }
         
          
     }
}
