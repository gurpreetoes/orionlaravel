<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;

class Category extends Model
{
    protected $fillable = [
        'category_name',
         'descr'      
    ];

    public static function getcat(){
        $categories = Category::all();
        return $categories;
    }

    public static function storeCat(CategoryRequest $request){      
       // dd($request);
      /// Create a new file for validate.
      $validated =  $request->validated();
        $category = new Category([
            'category_name' => $request->get('category_name'),
            'descr' => $request->get('descr')
        ]);
         $category->save();
    }

    public static function updateCat($request,$id){      
        $request->validate([
            'category_name'=>'required',
            'descr'=>'required'
        ]);
        $category = Category::find($id);
        $category->category_name =  $request->get('category_name');
        $category->descr = $request->get('descr');
        $category->save();
    }

    public static function destroyCat($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect('/admin/category')->with('success', 'Category deleted!');
    }

}
