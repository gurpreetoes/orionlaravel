<?php 
  Route::get('/', 'FrontendController@index');
Route::group(['middlewear'=>['auth']],function()
{
  
    Route::get('/frontend/home', 'FrontendController@index');
    Route::get('/category', 'FrontendController@getCat')->middleware('user');
    ;
    Route::get('/catDetail', 'FrontendController@catDetail')->middleware('user');;
    Route::get('/catdetail/{category_name}', [ 'as' => 'frontend.catdetail', 'uses' => 'FrontendController@catDetail'])->middleware('user');;
});
?>