<?php



Route::group(['middlewear'=>['auth']],function()
    {
        //dd('admin Midlewear');
    //in this area we just send those routes which is open after login .
    
        Route::get('/dashboard','AdminController@dashboard')->middleware('admin'); 
        
        // Category routes
        Route::get('/category/','CategoryController@index')->middleware('admin');
        Route::get('/category/create/','CategoryController@create')->middleware('admin');
        Route::post('/category/store', [ 'as' => 'category.store', 'uses' => 'CategoryController@store'])->middleware('admin');  
        Route::get('/category/edit/{id}', [ 'as' => 'category.edit', 'uses' => 'CategoryController@edit'])->middleware('admin');    
        Route::post('/category/update/{id}', [ 'as' => 'category.update', 'uses' => 'CategoryController@update'])->middleware('admin');
        Route::get('/category/delete/{id}', [ 'as' => 'category.destroy', 'uses' => 'CategoryController@destroy'])->middleware('admin');    
      
        // users routes
        Route::get('/users/','AdminController@displayuser')->middleware('admin');
        Route::get('/users/register/','AdminController@register')->middleware('admin');
        Route::post('/users/store', [ 'as' => 'users.store', 'uses' => 'AdminController@store'])->middleware('admin');    
        Route::get('/users/delete/{id}', [ 'as' => 'users.destroy', 'uses' => 'AdminController@destroy'])->middleware('admin');    
      
        //Email Routes
        Route::get('/email/','EmailController@index')->middleware('admin');
        Route::get('/email/send/','EmailController@sendEmail')->middleware('admin');
        Route::post('/email/store', [ 'as' => 'email.store', 'uses' => 'EmailController@store'])->middleware('admin');    
       
        
    });

?>