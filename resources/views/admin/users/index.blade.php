@extends('layouts.admin')
@section('content')
<div class="content">
        <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Manage Users</h4>
                @if(session()->get('success'))
                <div class="alert alert-success">
                  {{ session()->get('success') }}  
                </div>
                @endif
               <span style="float:right"><a  href="/admin/users/register/" class="btn btn-primary"> Add User</a></span>
              </div>
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <tr><th>
                        Name
                      </th>
                      <th>
                        Email
                      </th>
                      <th>
                        Phone
                      </th>
                      <th>
                        Address
                      </th>
                      <th>
                        Action
                      </th>
                    </tr></thead>
                   
                    @foreach($user as $users)
                    <tbody>
                      <tr>
                        <td>
                        {{$users->name}} 
                        </td>
                        <td>
                        {{$users->email}}
                        </td>
                        <td>
                        {{$users->phone}}
                        </td>
                        <td>
                        {{$users->address}}
                        </td>
                        <td> <a href="{{ route('users.destroy', $users->id)}}" class="btn btn-primary"> Delete</a>
                     
                     </td>
                      </tr>
                      @endforeach
                   
                    </tbody>
                  </table>
                  {{ $user->links() }}
                </div>
              </div>
            </div>
            
          </div>
        </div>
    </div>
@endsection