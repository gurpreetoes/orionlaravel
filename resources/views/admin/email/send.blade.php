@extends('layouts.admin')
@section('content')

<div class="row">
<div class="col-sm-12">
@if(session()->get('success'))
  <div class="alert alert-success">
    {{ session()->get('success') }}  
  </div>
@endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header">Send Email</div>
            @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div><br />
                @endif
            <div class="card-body">
                  <form method="post" action="{{ route('email.store') }}" class="forms-sample" enctype="multipart/form-data">
                  @csrf 
                  <div class="form-group">
                      <label for="exampleInputEmail3">To</label>
                      <input type="text" name="to" class="form-control" id="exampleInputName1"value="{{ old('to') }}" >
                    </div>
                    <div class="form-group">
                      <label >Subject</label>
                      <input type="text" name="subject" class="form-control" id="exampleInputEmail3"value="{{ old('subject') }}" >
                    </div>                    
                    <div class="form-group">
                      <label for="Message">Message</label>
                      <textarea  name="message" class="form-control" id="exampleTextarea1" rows="4">{{ old('message') }}</textarea>
                    </div>
                    <div class="form-group">
                      <label>Image upload</label>
                      <input type="file" name="image" >  
                    </div> 
                    <button type="submit" name="submit" class="btn btn-primary mr-2">Send Email</button>
                   </form>
                </div>
              </div>
            </div>
</div>
</div>
</div>
</div>



@endsection