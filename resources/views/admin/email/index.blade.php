@extends('layouts.admin')
@section('content')
<div class="content-fluid">
        <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Email Log</h4>
                @if(session()->get('success'))
                <div class="alert alert-success">
                  {{ session()->get('success') }}  
                </div>
                @endif
               <span style="float:right"><a  href="/admin/email/send/" class="btn btn-primary"> Send Email</a></span>
              </div>
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                    <tr>
                        <td width="200">To</td>
                        <td>Subject</td>          
                        <td>Message</td>
                        <td>Date</td>
                        <td>Attachment</td>
                      </tr></thead>
                                
                                
                      @foreach($dd as $d)  
                      <tr>
                          
                          <td width="200">{{$d->to}} </td>
                          <td>{{$d->subject}}</td>            
                          <td>{{$d->message}} </td>
                          <td>{{$d->created_at}} </td>
                          <td>
                          @if(empty($d->image))
                            <img src="{{asset('images/no-image.png')}}" height=150 width=150 >
                        @else            
                          <img src="{{asset('images/'.$d->image)}}" height=100 width=100 >            
                        @endif
                          </td>            
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  {{ $dd->links() }}
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

@endsection