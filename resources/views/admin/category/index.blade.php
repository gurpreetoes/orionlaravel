@extends('layouts.admin')
@section('content')
<div class="content">
        <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Manage Category</h4>
                @if(session()->get('success'))
                <div class="alert alert-success">
                  {{ session()->get('success') }}  
                </div>
                @endif
               <span style="float:right"><a  href="/admin/category/create/" class="btn btn-primary"> Add Category</a></span>
              </div>
             
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <tr><th>
                        Category
                      </th>
                      <th>
                        Description
                      </th>
                     <th> Action</th>
                    </tr></thead>
                    @foreach($categories as $category)
                    <tbody>
                      <tr>
                        <td>
                        {{$category->category_name}} 
                        </td>
                        <td width="400">
                        {!!$category->descr!!}
                        </td>
                       <td><a href="{{ route('category.edit',$category->id)}}" class="btn btn-primary"> Edit</a>
                       <a href="{{ route('category.destroy', $category->id)}}" class="btn btn-primary"> Delete</a>
                      </td>
                      </tr>
                      @endforeach

                    </tbody>
                  </table>
                  {{$categories->links()}}
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

@endsection