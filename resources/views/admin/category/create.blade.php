@extends('layouts.admin')
@section('content')

<div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Add Category</h5>
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div><br />
                @endif
              </div>
              <div class="card-body">
                <form action="{{ route('category.store') }}" method="post" >
                @csrf
                  <div class="row">
                    <div class="col-md-8 pr-1">
                      <div class="form-group">
                        <label>Category Name</label>
                        <input type="text" class="form-control" name="category_name" value="{{ old('category_name') }}">
                      </div>
                    </div>
                    </div>

                    <div class="row">
                    <div class="col-md-8 pr-1">
                      <div class="form-group">
                        <label>Description</label>
                        <textarea  class="form-control" id="editor1"  name="descr" style="background-color: transparent; border: 1px solid #E3E3E3; border-radius: 30px;" >{{ old('descr') }}</textarea>
                        <script>
                          CKEDITOR.replace( 'editor1' );
                      </script>
                      </div>
                    </div>
                    </div>

                    <div class="row">
                    <div class="col-md-8 pr-1">
                      <div class="form-group">
                       
                        <input type="submit" name="submit" value="Add Category" class="btn btn-primary">
                      </div>
                    </div>
                  </div>                         
                
                </form>
              </div>
            </div>
          </div>
       


@endsection