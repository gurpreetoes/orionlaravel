<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand/logo -->
  <a class="navbar-brand" href="#">Admin</a>
  
  <div style="width:1100px">&nbsp;</div>
    <div style="float:right">
    <ul class="navbar-nav">
    <li class="nav-item">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            </ul>
                            </div>
    
</nav>
<body>
  <div class="container" style="padding-top:20px">
    <div class="row">
      <div class="col-4">   
      <div class="card">
              <div class="card-header">  
              <h4 class="card-title">Dashboard</h4> 
                <ul>
                  <li><a href="/admin/users/">Manage Users</a></l1>
                  <li><a href="/admin/category/">Manage Category</a></li>
                  <li><a href="/admin/email">Manage Email</a></li>
                </ul>
              </div>
            </div>
      </div>
      <div class ="col-8">
       @yield('content')
      </div>
</div>
</body>
</html>