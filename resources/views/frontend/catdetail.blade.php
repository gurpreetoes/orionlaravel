@extends('layouts.frontend')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-4">
        <h1>Category</h1>
            <ul>
            @foreach ($categories as $categories) 
				<li><a href="{{ route('frontend.catdetail',$categories->category_name) }}"><?php echo $categories->category_name; ?></a></li>											
			@endforeach
            </ul>
        </div>
        <div class="col-8">
        @foreach ($catdetail as $catdetail)
            <h1><?php echo $catdetail->category_name; ?></h1>
            <p> <?php echo $catdetail->descr; ?> </p>
            @endforeach
        
        </div>
    </div>
</div>

@endSection